package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	
	public FindLeadsPage clickfindLead() {
		WebElement elefindLead = locateElement("linktext", "Find Leads");
		click(elefindLead);
		return new FindLeadsPage();
	}

	public CreateLeadPage clickCreateLead() {
		// TODO Auto-generated method stub
		WebElement eleCreateLead = locateElement("linktext","Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
}









