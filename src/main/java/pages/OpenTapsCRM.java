package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class OpenTapsCRM extends ProjectMethods{

	
	public OpenTapsCRM typeCompName(String data) {
		WebElement elecompName = locateElement("id", "updateLeadForm_companyName");
		type(elecompName, data);
		return this;
	}
	
	public FindLeadsPage clickupdate() {
		WebElement eleupdate = locateElement("class", "smallSubmit");
		click(eleupdate);
		return new FindLeadsPage();
	}
	
	}
	
