package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	
	public FindLeadsPage clickFindLeads() {
		WebElement eleFindLeads = locateElement("xapth", "//a[text()='Find Leads']");
		click(eleFindLeads);
		return this;
	}
	
	public FindLeadsPage clickPhone() {
		WebElement elePhone = locateElement("xpath", "//span[text()='Phone']");
		click(elePhone);
		return this;
	}
	
	public FindLeadsPage typephonenumber(String data) {
		WebElement elephonenumber = locateElement("name", "phoneNumber");
		type(elephonenumber, data);
		return this;
	}
	
	public FindLeadsPage clickfindLeads() throws InterruptedException {
		WebElement elefindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindLeads);
		Thread.sleep(3000);
		return this;
	}
	
	public FindLeadsPage clickfirstlead() throws InterruptedException {
		WebElement elefirstlead = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"); //a[@class='linktext'])[5]
		click(elefirstlead);
		Thread.sleep(3000);
		return this;
	}
	
	public OpenTapsCRM clickEdit() {
		WebElement eleEdit = locateElement("linktext", "Edit");
		click(eleEdit);
		return new OpenTapsCRM();
	}
	
	
	
	
	
}
