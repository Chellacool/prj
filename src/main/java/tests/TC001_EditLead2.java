package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC001_EditLead2 extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC002_EditLead";
		testCaseDescription ="Edit a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public  void EditLead(String ph, String compname) throws InterruptedException   {
		new MyHomePage()
		.clickLeads()
		.clickfindLead()
		.clickPhone()
		.typephonenumber(ph)
		.clickfindLeads()
		.clickfirstlead()
		.clickEdit()
		.typeCompName(compname)
		.clickupdate();
		
		
		
	}
	

}
