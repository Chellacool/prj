package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bankPages.BankBazaarFirstPage;
import wdMethods.bankPM;

public class BankBazaar extends bankPM {
	
	@BeforeClass
	public void setData() {
		testCaseName = "BankBazaarFirstPage";
		testCaseDescription ="SelectMutualFund";
		category = "Smoke";
		author= "Thala";
		//dataSheetName="TC001";
	}
	//@Test(dataProvider="fetchData")
	@Test
	public  void bank()   {
		new BankBazaarFirstPage()
		.mouseOverOnInvestment()
		.selectMutualFund()
		.ClickSearchMutFun()
		.SelAge()
		.SelYear()
		.SelDate()
		.ClickContinue()
		.SelSalary()
		.ClickCont()
		.ClickPrmAcc()
		.EnterFrstName()
		.clkviewmutualfund()
		.alertcancel()
		.getSchemes();
		
	}
	
}