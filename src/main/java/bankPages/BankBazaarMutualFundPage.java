package bankPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.bankPM;

public class BankBazaarMutualFundPage extends bankPM {

	int age = Integer.parseInt("26");
	
	public BankBazaarMutualFundPage ClickSearchMutFun() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locateElement("linktext","Search for Mutual Funds").click();
		return this;
	}

	public BankBazaarMutualFundPage SelAge() {
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Actions builder  = new Actions(driver);
		WebElement slider = driver.findElementByXPath("//div[@class='rangeslider__handle']");
		builder.dragAndDropBy(slider,(age-18)*8, 0).perform(); // 8 pixels for every age

		//locateElement("xpath","//div[@class='rangeslider__handle-label']").click();
		return this;
	}


	public BankBazaarMutualFundPage SelYear() {
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locateElement("xpath","//a[text()='Feb 1992']").click();
		return this;
	}

	public BankBazaarMutualFundPage SelDate() {
		locateElement("xpath","(//div[@class='react-datepicker__day react-datepicker__day--wed'])[2]").click();
		return this;
	}

	
	public BankBazaarMutualFundPage ClickContinue() {
		locateElement("xpath","//a[text()='Continue']").click();
		return this;
	}
	
	public BankBazaarMutualFundPage SelSalary() {
	//	Actions builder  = new Actions(driver);
	//	WebElement slider = driver.findElementByXPath("(//div[@class='rangeslider__handle-label'])/span");
		driver.findElementByName("netAnnualIncome").sendKeys("7,00,000");
		return this;
	}
	
	
	public BankBazaarMutualFundPage ClickCont() {
		locateElement("xpath","//a[text()='Continue']").click();
		return this;
	}
	
	public BankBazaarMutualFundPage ClickPrmAcc() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locateElement("xpath","(//input[@name='primaryBankAccount'])[2]").click();
		return this;
	}
	
	public BankBazaarMutualFundPage EnterFrstName() {
		locateElement("xpath","//input[@name='firstName']").sendKeys("Dinesh");
		return this;
	}
	
	public BankBazaarSchemesPage clkviewmutualfund() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		locateElement("linktext","View Mutual Funds").click();
		return new BankBazaarSchemesPage();
	}
	
		
	

	}


