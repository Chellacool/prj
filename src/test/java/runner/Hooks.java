package runner;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	@Before
	public void beforecucumber(Scenario sc) {
		String name = sc.getName();
		System.out.println("The test case name is:"+name);
		String id = sc.getId();	
		System.out.println(id);
		}
	@After
	public void aftercucumber(Scenario sc) {
		System.out.println("Status is:"+sc.getStatus());
	}

}
