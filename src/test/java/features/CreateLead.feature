Feature: CreateLead in Leaftap Application

#Background:
#
#Given launch the browser
#And maximize the browser
#And set the timeouts
#And enter the URL

Scenario: Positive flow for CreateLead
And enter the user name as DemoSalesManager
And enter the password as crmsfa
And clicks on the login button
And click on CRMSFA Link
And Click on CreateLead
And enter compName as Syntel
And enter FirstName as Chella
And enter LastName as art
When Click on CreateLeadSubmit
Then verify CreateLead Successfull


#Scenario Outline: Negative flow for CreateLead
#And enter the user name as <username>
#And enter the password as <password>
#And clicks on the login button
#And click on CRMSFA Link
#And Click on CreateLead
#And enter compName as Syntel
#And enter FirstName as Chella
#And enter LastName as art
#When Click on CreateLeadSubmit
#But Login Failed
#
#Examples: 
#
#|username|password|
#|DemoSalesManar|crmsfa|
#|DemoSalesManage|crmsfa|