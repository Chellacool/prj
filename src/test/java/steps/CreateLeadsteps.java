package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadsteps {

	ChromeDriver driver;

	@Given("launch the browser")
	public void launchBrowser(){
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@And("maximize the browser")
	public void maximize() {
		driver.manage().window().maximize();
	}

	@And("set the timeouts")
	public void timeouts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@And("enter the URL")
	public void url() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@And("enter the user name as (.*)")
	public void userName(String uName) {
		driver.findElementById("username").sendKeys(uName);
	}

	@And("enter the password as (.*)")
	public void password(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}

	@And("clicks on the login button")
	public void click() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@And("click on CRMSFA Link")
	public void clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@And("Click on CreateLead")
	public void clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@And("enter compName as (.*)")
	public void compName(String cmpName) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cmpName);
	}

	@And("enter FirstName as (.*)")
	public void FirstName(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
	}

	@And("enter LastName as (.*)")
	public void LastName(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@When("Click on CreateLeadSubmit")
	public void clickSubmit() {
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("verify CreateLead Successfull")
	public void CLSuccess() {
		System.out.println("Create Lead Successfull");
	}
	
	@But("Login Failed")
	public void failed() {
		System.out.println("Loing Failed");
	}
}







